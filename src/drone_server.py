import logging
import random
from threading import Thread

from flask import Flask, jsonify, request

from ardupilot.drone import Drone
from ardupilot.mission import Mission, MissionState
from server_utils import ok, error

app = Flask(__name__)
drone = Drone(log_level=logging.INFO)
logging.getLogger('werkzeug').setLevel(logging.WARNING)  # Disable server info logs


@app.route('/init', methods=['GET'])
def init():
	return ok('Init complete\n')


@app.route('/start_mission', methods=['POST'])
def start_mission():
	if drone.mission_state == MissionState.EXECUTING:
		return error('Mission is already executing')
	drone.mission_state = MissionState.EXECUTING

	mission = Mission(request.json)
	drone.start_mission(mission)

	# Start monitoring mission in the background
	Thread(target=drone.monitor_mission).start()

	return ok('Started the mission')


@app.route('/abort_mission', methods=['GET'])
def abort_mission():
	drone.abort_mission()
	return ok('Aborted the mission')


@app.route('/get_status', methods=['GET'])
def get_status():
	return jsonify(drone.get_status())  # serialize and use JSON headers


@app.route('/get_location', methods=['GET'])
def get_location():
	return jsonify(drone.get_location())  # serialize and use JSON headers


@app.route('/get_mission_status', methods=['GET'])
def get_mission_status():
	return jsonify(drone.get_mission_status())  # serialize and use JSON headers


@app.route('/get_rng_location', methods=['GET'])
def get_rng_location():
	return jsonify({
		# Whole world
		# 'latitude': random.randrange(-90, 90),
		# 'longitude': random.randrange(-180, 180)

		# Around Metu
		'latitude': random.uniform(35.247259, 35.248827),
		'longitude': random.uniform(33.019301, 33.021114)
	})


if __name__ == '__main__':
	app.run()

from flask import Response


def ok(message='', status=200):
	return Response(response=message, status=status)


def error(message, status=400):
	print('Error: ' + message)
	return Response(response='Error: ' + message, status=status)

import logging
import time

from dronekit import connect, VehicleMode

from ardupilot.drone_utils import Mode, Commands
from ardupilot.mission import Mission, MissionState

_logger = logging.getLogger(__name__)


class Drone:
	# Constructor
	def __init__(self, log_level=logging.WARNING):
		self.mission_state = MissionState.PENDING
		self._aborted = False
		self._mission_log = []

		# Init logger
		_logger.setLevel(log_level)

		# connection_string = '/dev/serial0' # real drone
		connection_string = 'tcp:192.168.31.3:5762'  # simulator on my PC
		self._vehicle = connect(connection_string, wait_ready=True)
		_logger.info(f'Connecting to the vehicle on: {connection_string}')

	# Destructor
	def __del__(self):
		self.close_connection()

	def close_connection(self):
		_logger.info('Closing connection')
		if self._vehicle:
			self._vehicle.close()

	def arm_motors(self):
		self._set_mode(Mode.GUIDED)
		self.log_status('Arming motors')
		while not self._vehicle.is_armable:
			time.sleep(1)
		self._vehicle.armed = True

	def takeoff(self, target_alt):
		self.log_status('Takeoff')
		self._vehicle.simple_takeoff(target_alt)

		# Keep polling altitude, until target altitude is reached
		while True:
			altitude = self._vehicle.location.global_relative_frame.alt

			if altitude >= target_alt - 1:
				self.log_status('Altitude reached')
				break

			time.sleep(0.1)

	def start_mission(self, mission: Mission):
		self._aborted = False
		self.clear_mission_log()
		self.arm_motors()
		self.takeoff(10)
		self.execute_mission(mission)

	def execute_mission(self, mission: Mission):
		self.log_status('Starting a mission')

		commands = self._vehicle.commands
		commands.clear()

		# Add a dummy command, since first command is deleted by dronekit for some reason
		commands.add(Commands.TAKEOFF.value)

		# Navigation commands
		for nav_command in mission.get_commands():
			commands.add(nav_command)

		# RTL command
		commands.add(Commands.RTL.value)

		commands.upload()

		# Setting mode to AUTO will start the mission
		self._vehicle.commands.next = 0
		self._set_mode(Mode.AUTO)

	def monitor_mission(self):
		last_command = 2
		next_command = 0
		command_count = self._vehicle.commands.count
		while next_command < command_count:
			next_command = self._vehicle.commands.next

			if next_command > last_command and not self._aborted:
				if next_command <= command_count:
					self.log_status(f'Reached waypoint {last_command - 1}')
					if next_command == command_count:
						self.log_status(f'Returning to launch')

				last_command = next_command
			time.sleep(0.1)

		# Wait until the drone is landed & disarmed
		while self._vehicle.armed:
			time.sleep(0.1)

		self.log_status("Landed & disarmed")
		self.mission_state = MissionState.FINISHED

	def get_status(self):
		return {
			'state': self._vehicle.system_status.state,
			'mode': self._vehicle.mode.name,
			'battery': self._vehicle.battery.level,
			'armed': self._vehicle.armed,
			'airspeed': self._vehicle.airspeed,
			'next_command': self._vehicle.commands.next,
			'location': {
				'latitude': self._vehicle.location.global_frame.lat,
				'longitude': self._vehicle.location.global_frame.lon,
				'altitude': self._vehicle.location.global_relative_frame.alt
			}
		}

	def abort_mission(self):
		# The last command is always RTL
		self.log_status(f'Aborting the mission')
		self._aborted = True
		self._vehicle.commands.next = self._vehicle.commands.count
		self._set_mode(Mode.RTL)

	def clear_mission_log(self):
		self._mission_log.clear()

	def get_mission_status(self):
		return {
			'mission_log': self._mission_log,
			'mission_state': self.mission_state.value
		}

	def get_location(self):
		return {
			'latitude': self._vehicle.location.global_frame.lat,
			'longitude': self._vehicle.location.global_frame.lon,
			'altitude': self._vehicle.location.global_relative_frame.alt
		}

	def land(self):
		self._set_mode(Mode.LAND)

	def _set_mode(self, mode: Mode):
		_logger.info(f'Setting mode to: {mode.value}')
		self._vehicle.mode = VehicleMode(mode.value)

	def log_status(self, message):
		self._mission_log.append(message)
		_logger.info(message)

import enum

from dronekit import Command
from pymavlink import mavutil


class Mode(enum.Enum):
	RTL = 'RTL'
	GUIDED = 'GUIDED'
	AUTO = 'AUTO'
	LAND = 'LAND'


def generate_command(cmd, lat=0, lon=0, alt=10):
	return Command(
		0, 0, 0,  # target_system, target_component, seq
		mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,  # frame
		cmd,  # command
		0, 0,  # current, autocontinue (both not supported)
		0, 0, 0, 0,  # command specific parameters
		lat,  # latitude
		lon,  # longitude
		alt  # altitude (depends on frame)
	)


class Commands(enum.Enum):
	TAKEOFF = generate_command(mavutil.mavlink.MAV_CMD_NAV_TAKEOFF)
	RTL = generate_command(mavutil.mavlink.MAV_CMD_NAV_RETURN_TO_LAUNCH)

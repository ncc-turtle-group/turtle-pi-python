from enum import Enum
from typing import Tuple, List

from pymavlink import mavutil

from ardupilot.drone_utils import generate_command


class MissionState(Enum):
	PENDING = 'Pending'
	EXECUTING = 'Executing'
	FINISHED = 'Finished'


class Mission:
	def __init__(self, waypoints):
		self._waypoints = list()
		for waypoint in waypoints:
			self.add_waypoint(waypoint['latitude'], waypoint['longitude'], waypoint['altitude'])

	def add_waypoint(self, lat: float, lon: float, alt: float):
		self._waypoints.append((lat, lon, alt))

	def add_waypoints(self, waypoints: List[Tuple]):
		self._waypoints.extend(waypoints)

	def get_commands(self):
		return map(lambda waypoint:
		           generate_command(mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, *waypoint), self._waypoints)
